cd cvodes
tar -xzf cvodes-2.7.0.tar.gz
cd cvodes-2.7.0
./configure --prefix=$PWD/cvodes
make
make install
cd ../
cd ../
tar -xzf StochKit2.0.10.tgz
# The Stochkit installer will get confused if there is already a StochKit installed
export STOCHKIT_HOME=
cd StochKit2.0.10
./install.sh
cd ../
export STOCHKIT_HOME=$PWD/StochKit2.0.10
export STOCHKIT_ODE=$PWD
echo $STOCHKIT_HOME
echo $STOCHKIT_ODE
make

echo ""
echo "########################"
echo "Set these environmental variables. Use this StochKit and ODE solver together. There are very likely version-sensitive issues"
echo "  export STOCHKIT_HOME=$PWD/StochKit2.0.10"
echo "  export STOCHKIT_ODE=$PWD"
